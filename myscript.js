var cars = [
	{
		id : 1,
		brand : "Audi",
		model : "C2 Class x",
		price : "80L",
		color : "red",
		isActive : true,
		variants : ["SUV", "Sedan", "mini SUV"],
		address : {
			home : "bangalore",
			office : "Delhi"
		},
		image : "https://imgd.aeplcdn.com/310x174/cw/ec/32595/Honda-Jazz-Facelift-Exterior-116465.jpg?wm=1&q=85"
	},
	{
		id : 2,
		brand : "Mercedes",
		model : "C3",
		price : "23L",
		color : "black",
		isActive : true,
		variants : ["Sedan", "mini SUV"],
		address : {
			home : "Mumbai",
			office : "Delhi"
		},
		image : "https://imgd.aeplcdn.com/310x174/cw/ec/32940/Hyundai-Santro-Exterior-118088.jpg?wm=0&q=85"
	},
	{
		id : 3,
		brand : "Bugati",
		model : "X class",
		price : "85L",
		color : "black",
		isActive : true,
		variants : ["Sedan"],
		address : {
			home : "Hyderabad",
			office : "Pune"
		},
		image : "https://imgd.aeplcdn.com/310x174/cw/ec/26523/Maruti-Suzuki-Ciaz-Facelift-Exterior-87489.jpg?wm=0&q=85"
	}
];


console.log(cars);


for (var i = 0; i < cars.length; i++) {
	var ui = `<div class="col-md-4">
	            <img src="`+cars[i].image+`">
	            <h3>`+cars[i].brand+` - `+ cars[i].model+`</h3>
	            <p>Price : `+ cars[i].price+`</p> 
	        </div>`;

	  $('#carsList').append(ui);
}